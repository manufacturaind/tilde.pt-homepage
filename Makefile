SSH_HOST=tilde.pt
SSH_PORT=22
SSH_USER=$$USER
SSH_TARGET_DIR=/home/rlafuente/webroot

serve:
	python3 -m http.server 8000

deploy: publish
	rsync -e "ssh -p $(SSH_PORT)" -P -rvzc --cvs-exclude --delete $(OUTPUTDIR)/ $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)

dry-deploy: publish
	rsync -n -e "ssh -p $(SSH_PORT)" -P -rvzc --cvs-exclude --delete $(OUTPUTDIR)/ $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)


.PHONY: serve deploy dry-deploy
